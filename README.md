# Feel The Groove #

Scans a local folder for all MP3 Files, reads the MP3 Tags and attempts to query the Spotify API to obtain the Spotify Song ID. Using this Spotify ID, you can retrieve the Audio Features of that song ID which include things like the Tempo, Feel, Loudness, etc.

## Proof Of Concept ##

The entire purpose of this is to add the 'play songs like this' in the Auditory Disruption App / Server.
The database will be contained on The Server, and the App will ask the server for a song like it.

## TODO ##

Efficiencies on track requests and processing large quantities of data.

Goal: Processing 32k Songs in a reasonable amount of time (like 2-3 minutes).
Current Status: 3773 Songs took 5:30 to process the Spotify Song IDs for Valid Tracks.

Build Status History:
31 Songs take around 17 seconds to retrieve the Song ID itself.