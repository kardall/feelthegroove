﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace FeelTheGroove {
    public partial class Form1 : Form {

        List<string> lstFilesFound = new List<string>();
        Rhythm r = null;
        List<string> lstBadFiles = new List<string>();
        private int goodTracks = 0;
        private int badTracks = 0;
        Dictionary<string, Rhythm.TrackFeatures> TrackDatabase = new Dictionary<string, Rhythm.TrackFeatures>();
        public Form1() {
            InitializeComponent();
            r = new Rhythm();
            lstFilesFound.Clear();
            lstBadFiles.Clear();
            TrackDatabase.Clear(); // TODO: Maybe load from existing database?
        }

        public void ResetProcessingStatus() {
            //Check if invoke requied if so return - as i will be recalled in correct thread
            if (ControlInvokeRequired(lblAnalyseStatus, () => ResetProcessingStatus())) return;
            lblAnalyseStatus.Text = "Processing...";
        }
        public void UpdateListBox(String text) {
            //Check if invoke requied if so return - as i will be recalled in correct thread
            if (ControlInvokeRequired(listBox1, () => UpdateListBox(text))) return;
            listBox1.Items.Add(text);
        }
        public void AnalysisStatusUpdate(bool isBad) {
            
            //Check if invoke requied if so return - as i will be recalled in correct thread
            if (ControlInvokeRequired(listBox1, () => AnalysisStatusUpdate(isBad))) return;
            if(isBad)
                badTracks++;
            else
                goodTracks++;
            lblAnalyseStatus.Text = string.Format("Track Status: {0} Good - {1} Bad",goodTracks,badTracks);
        }
        public void UpdateSongCount(String text) {
            //Check if invoke requied if so return - as i will be recalled in correct thread
            if (ControlInvokeRequired(lblSongCounterStatus, () => UpdateSongCount(text))) return;
            lblSongCounterStatus.Text = text;
        }
        public void UpdateProcessBar(int amount) {
            //Check if invoke requied if so return - as i will be recalled in correct thread
            if (ControlInvokeRequired(progressBar1, () => UpdateProcessBar(amount))) return;
            progressBar1.Increment(amount);
        }
        public void SetProgressBarMinMax(int minAmount, int maxAmount) {
            //Check if invoke requied if so return - as i will be recalled in correct thread
            if (ControlInvokeRequired(progressBar1, () => SetProgressBarMinMax(minAmount, maxAmount))) return;
            progressBar1.Maximum = maxAmount;
            progressBar1.Minimum = minAmount;
        }

        private void BtnSongSelect_Click(object sender, EventArgs e) {
            folderBrowserDialog1.RootFolder = Environment.SpecialFolder.Desktop;
            DialogResult res = folderBrowserDialog1.ShowDialog();
            lstFilesFound.Clear();
            goodTracks = 0;
            badTracks = 0;
            listBox1.Items.Clear();
            lblAnalyseStatus.Text = "Processing Status";
            if (res != DialogResult.OK) return;
            lstBadFiles.Clear();
            Task.Run(() => {
                DirSearch(@folderBrowserDialog1.SelectedPath);
                UpdateSongCount(String.Format("{0} Songs Found...", lstFilesFound.Count));
                ProcessSongs();
            });

        }

        public bool ControlInvokeRequired(Control c, Action a) {
            if (c.InvokeRequired) c.Invoke(new MethodInvoker(delegate { a(); }));
            else return false;

            return true;
        }

        private void ProcessSongs() {
            int maxSongs = lstFilesFound.Count;
            SetProgressBarMinMax(1,maxSongs-1);
            
                foreach(string file in lstFilesFound) {
                Task.Run(() => {
                    string text = r.GetTrackID(file);
                    if (text != string.Empty) {
                        UpdateProcessBar(1);
                        AnalysisStatusUpdate(false);
                        try {
                            TrackDatabase.Add(text, r.GetTrackFeatures(text));
                        } catch (ArgumentException) {
                            // Already Exists. Had to do it this way, because individual processes were having issues
                            // Finding if it exists if they add a track at the same time...
                        }
                    } else {
                        UpdateProcessBar(1);
                        AnalysisStatusUpdate(true);
                        UpdateListBox(file);
                    }
                });
                }
            
        }
        public void DirSearch(string sDir) {

            try {
                foreach (string f in Directory.GetFiles(sDir, "*.mp3", SearchOption.AllDirectories)) {
                    lstFilesFound.Add(f);
                }
            } catch (System.Exception excpt) {
                Console.WriteLine(excpt.Message);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e) {
            //string songID = listBox1.GetItemText(listBox1.SelectedItem);
            //ResetProcessingStatus();
            //try {
            //    Rhythm.TrackFeatures trackFeatureList = r.GetTrackFeatures(songID);
            //    if(trackFeatureList == null) {
            //        Console.WriteLine("Error... derp");
            //    }

                

            //} catch (Exception ex) {
            //    Console.WriteLine("Error: " + ex.Message);
            //}
        }
    }
}
