﻿Feel The Groove
(Song Analytics For Playlist Auto Creation)

Look up all songs
Get Track Information (Artist, Title)
Keep Track of Un-Tagged or Confusing Tracks for Update by User

Analyse the Song via the following qualities:

	Melody – the tune of the music
	Harmony – the chords and chord progression
	Rhythm – the beat and groove of the song
	Form/Song Structure – the different sections in the song
	Texture – the number of layers of music going on
	Timbre – the different qualities of the sounds used
	Dynamics – the ebb and flow / musical arch of the song
	Mix – the sonic qualities of the song

Melody Values 1 - Monotonous (Slow Classical) -> 10 - Craze Fest (Dubstep)
Harmony Values 1 - Pretty Standard, repeats the same progression in a patter, 10 - More Than 3 key changes
Rhythm Values 1 - Really slow (<60bpm) -> 10 - over 400bpm
Form/Song Structure Values 1 - one to two loops of verse or one verse/chorus -> 10 - Multiple Types of Changes (Dream Theater)
Texture Values 1 - Single Instrument -> 10 - Full wall of sound
Timbre Values 1 - Clear -> 10 - Heavily Distorted with no breaks
Dynamics Values 1 - No Volume Change -> 10 - Constant Flux more than 4 times during song
Mix Values (not sure, maybe this could be a combination of rhythm, melody and structure?)

