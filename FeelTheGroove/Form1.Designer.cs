﻿namespace FeelTheGroove {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.btnSongFolderSelect = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.lblSongCounterStatus = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lblAnalyseStatus = new System.Windows.Forms.Label();
            this.timerBPM = new System.Windows.Forms.Timer(this.components);
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSongFolderSelect
            // 
            this.btnSongFolderSelect.Location = new System.Drawing.Point(13, 13);
            this.btnSongFolderSelect.Name = "btnSongFolderSelect";
            this.btnSongFolderSelect.Size = new System.Drawing.Size(139, 23);
            this.btnSongFolderSelect.TabIndex = 0;
            this.btnSongFolderSelect.Text = "Select MP3 Folder";
            this.btnSongFolderSelect.UseVisualStyleBackColor = true;
            this.btnSongFolderSelect.Click += new System.EventHandler(this.BtnSongSelect_Click);
            // 
            // lblSongCounterStatus
            // 
            this.lblSongCounterStatus.AutoSize = true;
            this.lblSongCounterStatus.Location = new System.Drawing.Point(158, 18);
            this.lblSongCounterStatus.Name = "lblSongCounterStatus";
            this.lblSongCounterStatus.Size = new System.Drawing.Size(0, 13);
            this.lblSongCounterStatus.TabIndex = 1;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(13, 42);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(314, 10);
            this.progressBar1.TabIndex = 2;
            // 
            // lblAnalyseStatus
            // 
            this.lblAnalyseStatus.AutoSize = true;
            this.lblAnalyseStatus.Location = new System.Drawing.Point(10, 55);
            this.lblAnalyseStatus.Name = "lblAnalyseStatus";
            this.lblAnalyseStatus.Size = new System.Drawing.Size(92, 13);
            this.lblAnalyseStatus.TabIndex = 3;
            this.lblAnalyseStatus.Text = "Processing Status";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 103);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(663, 225);
            this.listBox1.TabIndex = 5;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Files That Need Correcting";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 333);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.lblAnalyseStatus);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lblSongCounterStatus);
            this.Controls.Add(this.btnSongFolderSelect);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSongFolderSelect;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label lblSongCounterStatus;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lblAnalyseStatus;
        private System.Windows.Forms.Timer timerBPM;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label1;
    }
}

