﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SimpleJson;
using TagLib;

/// <summary>
/// Calculates BPM of Song
/// </summary>
namespace FeelTheGroove
{
    public class Rhythm {
        public string SongBPM = String.Empty;
        private string myToken = null;
        private string clientid = "your_client_id";
        private string clientsecret = "your_client_secret";
        public Rhythm() {
        }

        #region Spotify Track Info Class [ TrackInfoObject ]
        /*
         * json2csharp class converted
         */
        public class ExternalUrls {
            public string spotify { get; set; }
        }

        public class Artist {
            public ExternalUrls external_urls { get; set; }
            public string href { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public string type { get; set; }
            public string uri { get; set; }
        }

        public class ExternalUrls2 {
            public string spotify { get; set; }
        }

        public class Image {
            public int height { get; set; }
            public string url { get; set; }
            public int width { get; set; }
        }

        public class Album {
            public string album_type { get; set; }
            public List<Artist> artists { get; set; }
            public List<string> available_markets { get; set; }
            public ExternalUrls2 external_urls { get; set; }
            public string href { get; set; }
            public string id { get; set; }
            public List<Image> images { get; set; }
            public string name { get; set; }
            public string type { get; set; }
            public string uri { get; set; }
        }

        public class ExternalUrls3 {
            public string spotify { get; set; }
        }

        public class Artist2 {
            public ExternalUrls3 external_urls { get; set; }
            public string href { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public string type { get; set; }
            public string uri { get; set; }
        }

        public class ExternalIds {
            public string isrc { get; set; }
        }

        public class ExternalUrls4 {
            public string spotify { get; set; }
        }

        public class Item {
            public Album album { get; set; }
            public List<Artist2> artists { get; set; }
            public List<string> available_markets { get; set; }
            public int disc_number { get; set; }
            public int duration_ms { get; set; }
            public bool @explicit { get; set; }
            public ExternalIds external_ids { get; set; }
            public ExternalUrls4 external_urls { get; set; }
            public string href { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public int popularity { get; set; }
            public string preview_url { get; set; }
            public int track_number { get; set; }
            public string type { get; set; }
            public string uri { get; set; }
        }

        public class Tracks {
            public string href { get; set; }
            public List<Item> items { get; set; }
            public int limit { get; set; }
            public string next { get; set; }
            public int offset { get; set; }
            public object previous { get; set; }
            public int total { get; set; }
        }

        public class TrackInfoObject {
            public Tracks tracks { get; set; }
        }
        #endregion

        #region Spotify Track Features Class [ TrackFeatures ]
        public class TrackFeatures {
            public double danceability { get; set; }
            public double energy { get; set; }
            public int key { get; set; }
            public double loudness { get; set; }
            public int mode { get; set; }
            public double speechiness { get; set; }
            public double acousticness { get; set; }
            public double instrumentalness { get; set; }
            public double liveness { get; set; }
            public double valence { get; set; }
            public double tempo { get; set; }
            public string type { get; set; }
            public string id { get; set; }
            public string uri { get; set; }
            public string track_href { get; set; }
            public string analysis_url { get; set; }
            public int duration_ms { get; set; }
            public int time_signature { get; set; }
        }
        #endregion

        public string GetTrackID(string fileName) {
            var tagFile = TagLib.File.Create(@fileName);
            Tag tag = tagFile.GetTag(TagTypes.Id3v2);
            string trackArtist = Uri.EscapeDataString(tag.JoinedPerformers);
            string trackTitle = Uri.EscapeDataString(tag.Title);
            string trackAlbum = tag.Album;
            
            //trackTitle = System.Uri.EscapeUriString(trackTitle);
            //trackArtist = System.Uri.EscapeUriString(trackArtist);
            string url = string.Format("https://api.spotify.com/v1/search?q=");
            if(trackArtist != null && trackTitle != null) {
                url = url + trackTitle.Replace(' ', '+') + " " + trackArtist.Replace(' ', '+');
            } else {
                if(trackArtist == null) {
                    url = url + trackArtist.Replace(' ', '+');
                } else {
                    url = url + trackTitle.Replace(' ', '+');
                }
            }
            url = url + string.Format("&type=track&market=US&limit=10&offset=0");

            //Uri spotifySearchQueryUrl = new Uri(url);
            string result = GetTrackInfo(url);
            //Console.WriteLine(result);
            // we are getting 10 tracks we need to filter by most popular (most likely the one)
            string id = "";
            int popularity = 0;

            TrackInfoObject tList = JsonConvert.DeserializeObject<TrackInfoObject>(result);
            if (tList.tracks == null) {
                Console.WriteLine("Cannot Retrieve Song: {0} - {1} : {2}", trackArtist, trackTitle, fileName);
            } else {
                foreach (Item t in tList.tracks.items) {
                    if (id == "") {
                        id = t.id;
                        popularity = t.popularity;
                    } else {
                        if (t.popularity > popularity) {
                            id = t.id;
                            popularity = t.popularity;
                        }
                    }
                }
            }

            if (id == "") {
                Console.WriteLine("Error Retrieving {0} - {1} ", trackArtist, trackTitle);
                Console.WriteLine("Url: " + url);
                
            }
            return id;
        }

        public TrackFeatures GetTrackFeatures(string song_id) {
            myToken = GetAccessToken(); // Potential Delay. Maybe check token for expiry somehow
            string url = string.Format("https://api.spotify.com/v1/audio-features/{0}", song_id);
            string webResponse = string.Empty;
            try {
                HttpClient hc = new HttpClient();
                var request = new HttpRequestMessage() {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Get
                };
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                request.Headers.Authorization = new AuthenticationHeaderValue("Authorization", "Bearer " + myToken);
                var task = hc.SendAsync(request)
                    .ContinueWith((taskwithmsg) => {
                        var response = taskwithmsg.Result;
                        var jsonTask = response.Content.ReadAsStringAsync();
                        webResponse = jsonTask.Result;
                    });
                task.Wait();

            } catch (WebException ex) {
                Console.WriteLine("Track Request Error: " + ex.Status);
            }
            return JsonConvert.DeserializeObject<TrackFeatures>(webResponse);
        }

        private string GetTrackInfo(string url) {
            string webResponse = string.Empty;
            myToken = GetAccessToken();
            try {
                // Get token for request
                HttpClient hc = new HttpClient();
                var request = new HttpRequestMessage() {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Get
                };
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                request.Headers.Authorization = new AuthenticationHeaderValue("Authorization", "Bearer " + myToken);
                // TODO: Had a task cancelled here, too many errors. Find a better way to process this maybe.
                // Same code is in GetTrackFeatures function.
                var task = hc.SendAsync(request)
                    .ContinueWith((taskwithmsg) => {
                        var response = taskwithmsg.Result;
                        var jsonTask = response.Content.ReadAsStringAsync();
                        webResponse = jsonTask.Result;
                    });
                task.Wait();

            } catch (WebException ex) {
                Console.WriteLine("Track Request Error: " + ex.Status);
            }
            return webResponse;
        }
        
        public class SpotifyToken {
            public string Access_token { get; set; }
            public string Token_type { get; set; }
            public int Expires_in { get; set; }
        }

        public string GetAccessToken() {
            SpotifyToken token = new SpotifyToken();
            string url5 = "https://accounts.spotify.com/api/token";
            //var clientid = "your_client_id";
            //var clientsecret = "your_client_secret";

            //request to get the access token
            var encode_clientid_clientsecret = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("{0}:{1}", clientid, clientsecret)));

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url5);

            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Accept = "application/json";
            webRequest.Headers.Add("Authorization: Basic " + encode_clientid_clientsecret);

            var request = ("grant_type=client_credentials");
            byte[] req_bytes = Encoding.ASCII.GetBytes(request);
            webRequest.ContentLength = req_bytes.Length;

            Stream strm = webRequest.GetRequestStream();
            strm.Write(req_bytes, 0, req_bytes.Length);
            strm.Close();

            HttpWebResponse resp = (HttpWebResponse)webRequest.GetResponse();
            String json = "";
            using (Stream respStr = resp.GetResponseStream()) {
                using (StreamReader rdr = new StreamReader(respStr, Encoding.UTF8)) {
                    //should get back a string i can then turn to json and parse for accesstoken
                    json = rdr.ReadToEnd();
                    rdr.Close();
                }
            }
            token = JsonConvert.DeserializeObject<SpotifyToken>(json);
            return token.Access_token;
        }

        #region directory functions

        // Test Async Style Dir Search
        public IEnumerable<string> IterateFiles(string path, string pattern) {
            var entryQueue = new Queue<string>();
            entryQueue.Enqueue(path);

            while (entryQueue.Count > 0) {
                var subdirs = Directory.GetDirectories(entryQueue.Peek());
                var files = Directory.GetFiles(entryQueue.Peek(), pattern, SearchOption.TopDirectoryOnly);
                foreach (var file in files)
                    yield return file;
                entryQueue.Dequeue();

                foreach (var subdir in subdirs)
                    entryQueue.Enqueue(subdir);
            }
        }
        //public List<string> DirSearch(string sDir) {
            
        //    try {
        //        foreach (string d in Directory.GetDirectories(sDir)) {
        //            foreach (string f in Directory.GetFiles(d)) {
        //                lstFilesFound.Items.Add(f);
        //            }
        //            DirSearch(d);
        //        }
        //    } catch (System.Exception excpt) {
        //        Console.WriteLine(excpt.Message);
        //    }
        //}
        //static int SongIDCounter;
        //public static List<string> DirSearch(string sDir) {
        //    List<string> tmp = new List<string>();
        //    try {
        //        string[] songs = Directory.GetFiles(sDir, "*.mp3", SearchOption.AllDirectories);
        //        foreach (string file in songs) {
        //            tmp.Add(file);
        //            SongIDCounter++;
        //        }

        //    } catch (Exception excpt) {

        //        Console.WriteLine(excpt.Message);
        //    }
        //    return tmp;
        //}
        #endregion



    }
}
